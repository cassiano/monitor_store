import React from 'react'
import { useState } from 'react'
import { useBrandsLoader, useScreenSizesLoader } from '../hooks/loaders'
import { AddMonitorFunctionType, MonitorType } from './MonitorStore'

type MonitorDefaultFormDataType = {
  productNumber: string
  brandId: number | ''
  screenSizeId: number | ''
  price: number | ''
  quantity: number | ''
}

const MONITOR_DEFAULT_FORM_DATA: MonitorDefaultFormDataType = {
  productNumber: '',
  brandId: '',
  screenSizeId: '',
  price: '',
  quantity: 0,
}

type NewMonitorFormProps = {
  onAddMonitor: AddMonitorFunctionType
}

export const NewMonitorForm: React.FC<NewMonitorFormProps> = ({
  onAddMonitor,
}) => {
  const [monitorFormData, setMonitorFormData] = useState(
    MONITOR_DEFAULT_FORM_DATA
  )

  const { data: brands } = useBrandsLoader()
  const { data: screenSizes } = useScreenSizesLoader()

  const handleAddMonitorButtonClick = (
    _event: React.MouseEvent<HTMLElement>
  ) => {
    onAddMonitor(monitorFormData as MonitorType)
      .then(() =>
        // Monitor added successfully! Reset the form.
        setMonitorFormData(MONITOR_DEFAULT_FORM_DATA)
      )
      .catch(err => alert(err))
  }

  return (
    <fieldset>
      <legend>Add to inventory</legend>

      <label htmlFor='productNumber'>Product Number: </label>
      <input
        type='text'
        name='productNumber'
        value={monitorFormData.productNumber}
        onChange={e =>
          setMonitorFormData({
            ...monitorFormData,
            productNumber: e.target.value,
          })
        }
      />
      <br />

      <label htmlFor='brand'>Brand: </label>
      <select
        name='brand'
        value={monitorFormData.brandId}
        onChange={e =>
          setMonitorFormData({
            ...monitorFormData,
            brandId: e.target.value as MonitorDefaultFormDataType['brandId'],
          })
        }
      >
        <option value=''>-- Select --</option>
        {brands.map(brand => (
          <option value={brand.id} key={brand.id}>
            {brand.name}
          </option>
        ))}
      </select>
      <br />

      <label htmlFor='screenSize'>Screen Size: </label>
      <select
        name='screenSize'
        value={monitorFormData.screenSizeId}
        onChange={e =>
          setMonitorFormData({
            ...monitorFormData,
            screenSizeId: parseInt(
              e.target.value
            ) as MonitorDefaultFormDataType['screenSizeId'],
          })
        }
      >
        <option value=''>-- Select --</option>
        {screenSizes.map(screenSize => (
          <option value={screenSize.id} key={screenSize.id}>
            {screenSize.value}
          </option>
        ))}
      </select>
      <br />

      <label htmlFor='price'>Price: </label>
      <input
        type='number'
        name='price'
        value={monitorFormData.price}
        onChange={e =>
          setMonitorFormData({
            ...monitorFormData,
            price: e.target.value !== '' ? parseFloat(e.target.value) : '',
          })
        }
      />
      <br />

      <label htmlFor='quantity'>Quantity: </label>
      <input
        type='number'
        name='quantity'
        value={monitorFormData.quantity}
        onChange={e =>
          setMonitorFormData({
            ...monitorFormData,
            quantity: e.target.value !== '' ? parseInt(e.target.value) : '',
          })
        }
      />
      <br />

      <input
        type='button'
        value='Add Monitor'
        onClick={handleAddMonitorButtonClick}
        disabled={
          monitorFormData.productNumber === '' ||
          monitorFormData.brandId === '' ||
          monitorFormData.price === '' ||
          monitorFormData.price <= 0 ||
          monitorFormData.quantity === '' ||
          monitorFormData.quantity <= 0
        }
      />
    </fieldset>
  )
}
