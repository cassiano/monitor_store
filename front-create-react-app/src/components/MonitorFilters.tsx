import React from 'react'
import { BrandType } from './MonitorStore'
import { useBrandsLoader, useScreenSizesLoader } from '../hooks/loaders'
import {
  MonitorFiltersType,
  ChangeFilterValueFunctionType,
  ToggleFilterFunctionType,
  MonitorType,
  ScreenSizeType,
} from './MonitorStore'

type MonitorFiltersProps = {
  monitorFilters: MonitorFiltersType
  monitors: MonitorType[]
  onToggleFilter: ToggleFilterFunctionType
  onChangeFilterValue: ChangeFilterValueFunctionType
}

export const MonitorFilters: React.FC<MonitorFiltersProps> = ({
  monitorFilters,
  monitors,
  onToggleFilter,
  onChangeFilterValue,
}) => {
  const { data: brands } = useBrandsLoader()
  const { data: screenSizes } = useScreenSizesLoader()

  const uniqueMonitorsFields = (field: keyof MonitorType) => [
    ...new Set(monitors.map(monitor => monitor[field])),
  ]

  // Find unique values within all monitors.
  const uniqueMonitorBrands = uniqueMonitorsFields(
    'brandId'
  ) as BrandType['id'][]
  const uniqueMonitorScreenSizes = uniqueMonitorsFields(
    'screenSizeId'
  ) as ScreenSizeType['id'][]

  const findBrandById = (id: number) => brands.find(brand => brand.id === id)
  const findScreenSizeById = (id: number) =>
    screenSizes.find(screenSize => screenSize.id === id)

  return (
    <fieldset>
      <legend>Filters</legend>

      <input
        type='checkbox'
        name='brandFilterChecked'
        checked={monitorFilters.brandId.active}
        onChange={onToggleFilter('brandId')}
      />
      <label htmlFor='brandFilterValue'>Brand: </label>
      <select
        name='brandFilterValue'
        value={monitorFilters.brandId.value}
        onChange={onChangeFilterValue('brandId')}
      >
        <option value=''>-- Select -- </option>
        {uniqueMonitorBrands.map(id => (
          <option value={id} key={id}>
            {findBrandById(id)?.name}
          </option>
        ))}
      </select>
      <br />

      <input
        type='checkbox'
        name='minimumSizeFilterChecked'
        checked={monitorFilters.minimumScreenSize.active}
        onChange={onToggleFilter('minimumScreenSize')}
      />
      <label htmlFor='minimumScreenSizeFilterValue'>
        Minimum Screen Size:{' '}
      </label>
      <select
        name='minimumScreenSizeFilterValue'
        value={monitorFilters.minimumScreenSize.value}
        onChange={onChangeFilterValue('minimumScreenSize')}
      >
        <option value=''>-- Select -- </option>
        {uniqueMonitorScreenSizes.map(id => {
          const screenSize = findScreenSizeById(id)

          return (
            <option value={screenSize?.value} key={id}>
              {screenSize?.value}
            </option>
          )
        })}
      </select>
      <br />

      <input
        type='checkbox'
        name='maximumPriceFilterChecked'
        checked={monitorFilters.maximumPrice.active}
        onChange={onToggleFilter('maximumPrice')}
      />
      <label htmlFor='maximumPriceFilterValue'>Maximum Price: </label>
      <input
        type='number'
        name='maximumPriceFilterValue'
        value={monitorFilters.maximumPrice.value}
        onChange={onChangeFilterValue('maximumPrice')}
      />
      <br />
    </fieldset>
  )
}
