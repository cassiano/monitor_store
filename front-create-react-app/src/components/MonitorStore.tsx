import React from 'react'
import { useState } from 'react'
import { MonitorInventory } from './MonitorInventory'
import { MonitorFilters } from './MonitorFilters'
import { NewMonitorForm } from './NewMonitorForm'
import { useMonitorsLoader } from '../hooks/loaders'
import { createMonitorApi, sellMonitorApi } from '../services/api'

export type ScreenSizeType = {
  id: number
  value: number
}

export type BrandType = {
  id: number
  name: string
}

export type MonitorType = {
  id: number
  productNumber: string
  brandId: BrandType['id']
  brand: BrandType
  screenSizeId: ScreenSizeType['id']
  screenSize: ScreenSizeType
  price: number
  quantity: number
}

export type MonitorFiltersType = {
  brandId: { active: boolean; value: BrandType['id'] | '' }
  minimumScreenSize: { active: boolean; value: ScreenSizeType['value'] | '' }
  maximumPrice: { active: boolean; value: number | '' }
}

export type ToggleFilterFunctionType = (
  fieldName: keyof MonitorFiltersType
) => (event: React.ChangeEvent<HTMLInputElement>) => void

export type ChangeFilterValueFunctionType = (
  fieldName: keyof MonitorFiltersType
) => (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void

export type AddMonitorFunctionType = (
  newMonitorData: MonitorType
) => Promise<void>

export type SellMonitorFunctionType = (productNumber: string) => Promise<number>

const DEFAULT_MONITOR_FILTERS: MonitorFiltersType = {
  brandId: { active: false, value: '' },
  minimumScreenSize: { active: false, value: '' },
  maximumPrice: { active: false, value: '' },
}

export const MonitorStore: React.FC = () => {
  const [monitorFilters, setMonitorFilters] = useState(DEFAULT_MONITOR_FILTERS)
  const { data: monitors, setData: setMonitors } = useMonitorsLoader()

  const inStockMonitors = () => monitors.filter(monitor => monitor.quantity > 0)

  const visibleMonitors = () => {
    let filteredMonitors = inStockMonitors()

    if (monitorFilters.brandId.active && monitorFilters.brandId.value !== '')
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.brandId === +monitorFilters.brandId.value
      )

    if (
      monitorFilters.minimumScreenSize.active &&
      monitorFilters.minimumScreenSize.value !== ''
    )
      filteredMonitors = filteredMonitors.filter(
        monitor =>
          monitor.screenSize.value >= +monitorFilters.minimumScreenSize.value
      )

    if (
      monitorFilters.maximumPrice.active &&
      monitorFilters.maximumPrice.value !== ''
    )
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.price <= +monitorFilters.maximumPrice.value
      )

    return filteredMonitors
  }

  const handleAddMonitorButtonClick: AddMonitorFunctionType =
    newMonitorData => {
      const monitorAlreadyExists =
        monitors.find(
          monitor => monitor.productNumber === newMonitorData.productNumber
        ) !== undefined

      if (monitorAlreadyExists)
        return Promise.reject(
          `Sorry, the product number "${newMonitorData.productNumber}" is already in use.`
        )

      createMonitorApi(newMonitorData)
        .then(response =>
          setMonitors(previousMonitors =>
            previousMonitors.concat(response.data)
          )
        )
        .catch(console.log)

      return Promise.resolve()
    }

  const handleSellMonitor: SellMonitorFunctionType = productNumber => {
    const soldMonitor = monitors.find(
      monitor => monitor.productNumber === productNumber && monitor.quantity > 0
    )

    if (!soldMonitor)
      return Promise.reject(
        `Monitor with product number "${productNumber}" not found`
      )

    sellMonitorApi(soldMonitor)
      .then(response =>
        setMonitors(previousMonitors =>
          previousMonitors.map(monitor =>
            monitor.id === soldMonitor.id ? response.data : monitor
          )
        )
      )
      .catch(console.log)

    return Promise.resolve(soldMonitor.quantity - 1)
  }

  const handleToggleFilter: ToggleFilterFunctionType = fieldName => event => {
    setMonitorFilters({
      ...monitorFilters,
      [fieldName]: {
        ...monitorFilters[fieldName],
        active: event.target.checked,
      },
    })
  }

  const handleChangeFilterValue: ChangeFilterValueFunctionType =
    fieldName => event => {
      setMonitorFilters({
        ...monitorFilters,
        [fieldName]: {
          ...monitorFilters[fieldName],
          value: parseInt(event.target.value),
        },
      })
    }

  return (
    <div className='App'>
      <header className='App-header'>
        <NewMonitorForm onAddMonitor={handleAddMonitorButtonClick} />

        <br />
        <MonitorFilters
          monitorFilters={monitorFilters}
          monitors={inStockMonitors()}
          onToggleFilter={handleToggleFilter}
          onChangeFilterValue={handleChangeFilterValue}
        />

        <br />
        <h2>Current Inventory:</h2>
        <MonitorInventory
          monitors={visibleMonitors()}
          onSellMonitor={handleSellMonitor}
        />
      </header>
    </div>
  )
}
