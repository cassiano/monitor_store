import React from 'react'
import { MonitorType, SellMonitorFunctionType } from './MonitorStore'
import { useState } from 'react'

type MonitorInventoryProps = {
  monitors: MonitorType[]
  onSellMonitor: SellMonitorFunctionType
}

export const MonitorInventory: React.FC<MonitorInventoryProps> = ({
  monitors,
  onSellMonitor,
}) => {
  const [selectedMonitorProductNumber, setSelectedMonitorProductNumber] =
    useState<string | null>(null)

  const handleSellMonitorButtonClick =
    (productNumber: string) => (_event: React.MouseEvent<HTMLElement>) => {
      if (
        window.confirm(
          `Are you sure you want to sell monitor with product number "${productNumber}"?`
        )
      )
        onSellMonitor(productNumber)
          .then(updatedQuantity => {
            // Monitor sold out after this sale?
            if (updatedQuantity === 0) setSelectedMonitorProductNumber(null)
          })
          .catch(err => alert(err))
    }

  return (
    <>
      <table border={1} cellPadding='3' style={{ fontSize: 12 }}>
        <thead>
          <tr>
            <th>Product Number</th>
            <th>Brand</th>
            <th>Screen Size</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          {monitors.map(monitor => (
            <tr
              style={{
                borderColor:
                  selectedMonitorProductNumber === monitor.productNumber
                    ? 'red'
                    : 'white',
              }}
              key={monitor.productNumber}
              onClick={_event =>
                setSelectedMonitorProductNumber(monitor.productNumber)
              }
            >
              <td>{monitor.productNumber}</td>
              <td>{monitor.brand.name}</td>
              <td>{monitor.screenSize.value}</td>
              <td>{monitor.price.toFixed(2)}</td>
              <td>{monitor.quantity}</td>
            </tr>
          ))}
        </tbody>
      </table>

      {selectedMonitorProductNumber !== null && (
        <>
          <br />
          <input
            type='button'
            value={`Sell "${selectedMonitorProductNumber}" Monitor`}
            onClick={handleSellMonitorButtonClick(
              selectedMonitorProductNumber!
            )}
          />
        </>
      )}
    </>
  )
}
