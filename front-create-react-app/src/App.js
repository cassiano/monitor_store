import './App.css'
import { MonitorStore } from './components/MonitorStore'

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <MonitorStore />
      </header>
    </div>
  )
}

export default App
