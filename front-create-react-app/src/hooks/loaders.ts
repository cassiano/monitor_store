import { useEffect, useState } from 'react'
import {
  listBrandsApi,
  listScreenSizesApi,
  listMonitorsApi,
} from '../services/api'
import {
  BrandType,
  MonitorType,
  ScreenSizeType,
} from '../components/MonitorStore'
import { AxiosResponse } from 'axios'

const useGenericListLoader = <T>(
  initialValue: T,
  listApi: () => Promise<AxiosResponse<T, any>>
) => {
  const [data, setData] = useState<T>(initialValue)

  useEffect(() => {
    listApi()
      .then(response => setData(response.data))
      .catch(console.log)
  }, [listApi])

  return { data, setData }
}

export const useBrandsLoader = () => {
  return useGenericListLoader<BrandType[]>([], listBrandsApi)
}

export const useScreenSizesLoader = () => {
  return useGenericListLoader<ScreenSizeType[]>([], listScreenSizesApi)
}

export const useMonitorsLoader = () => {
  return useGenericListLoader<MonitorType[]>([], listMonitorsApi)
}
