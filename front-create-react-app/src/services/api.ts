import { ScreenSizeType } from './../components/MonitorStore'
import axios from 'axios'
import { MonitorType, BrandType } from '../components/MonitorStore'

axios.defaults.baseURL = 'http://localhost:3001/api'

export const listBrandsApi = () => axios.get<BrandType[]>('/brands')

export const listScreenSizesApi = () =>
  axios.get<ScreenSizeType[]>('/screen_sizes')

export const listMonitorsApi = () =>
  axios.get<MonitorType[]>('/computer_monitors')
export const createMonitorApi = (attrs: MonitorType) =>
  axios.post<MonitorType>('/computer_monitors', attrs)
export const sellMonitorApi = (monitor: MonitorType) =>
  axios.patch<MonitorType>(`/computer_monitors/${monitor.id}/sell`)
