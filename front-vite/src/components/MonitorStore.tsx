import { MonitorFilters } from './MonitorFilters'
import { MonitorInventory } from './MonitorInventory'
import { NewMonitorForm } from './NewMonitorForm'
import { useState } from 'react'
import { useMonitorsLoader } from '../services/loaders'
import { createMonitorApi, sellMonitorApi } from '../services/api'
import React from 'react'

// export const BRAND_NAMES = ['Dell', 'HP', 'IBM', 'Lenovo'] as const
// export const SCREEN_SIZES = [17, 19, 21, 23, 25] as const

// export type BrandNameType = typeof BRAND_NAMES[number]
// export type ScreenSizeType = typeof SCREEN_SIZES[number]

export type BrandType = {
  id: number
  name: string
}

export type ScreenSizeType = {
  id: number
  value: number
}

export type MonitorType = {
  id: number
  productNumber: string
  brandId: number
  brand: BrandType
  screenSizeId: number
  screenSize: ScreenSizeType
  price: number
  quantity: number
}

export type MonitorFiltersType = {
  brand: { active: boolean; value: number | '' }
  minimumScreenSize: { active: boolean; value: ScreenSizeType | '' }
  maximumPrice: { active: boolean; value: number | '' }
}

const DEFAULT_MONITOR_FILTERS: MonitorFiltersType = {
  brand: { active: false, value: '' },
  minimumScreenSize: { active: false, value: '' },
  maximumPrice: { active: false, value: '' },
}

export type AddMonitorFunctionType = (monitorData: MonitorType) => Promise<void>

export type ToggleFilterFunctionType = (
  fieldName: keyof MonitorFiltersType
) => (event: React.ChangeEvent<HTMLInputElement>) => void

export type ChangeFilterValueFunctionType = (
  fieldName: keyof MonitorFiltersType
) => (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void

export type SellMonitorFunctionType = (productNumber: string) => Promise<number>

// const FILTERS_DATA: readonly [
//   keyof MonitorFiltersType,
//   keyof MonitorType,
//   (a: any, b: any) => boolean
// ][] = [
//   ['brand', 'brand', (a: BrandNameType, b: BrandNameType) => a === b],
//   [
//     'minimumScreenSize',
//     'screenSize',
//     (a: ScreenSizeType, b: ScreenSizeType) => a >= b,
//   ],
//   ['maximumPrice', 'price', (a: number, b: number) => a <= b],
// ]

type MonitorStoreProps = {
  storeName: string
}

export const MonitorStore: React.FC<MonitorStoreProps> = ({ storeName }) => {
  const { monitors, setMonitors } = useMonitorsLoader()

  const [monitorFilters, setMonitorFilters] = useState(DEFAULT_MONITOR_FILTERS)

  const inStockMonitors = () => monitors.filter(monitor => monitor.quantity > 0)

  const handleAddMonitor: AddMonitorFunctionType = monitorData => {
    const monitorAlreadyExists =
      monitors.find(
        monitor => monitor.productNumber === monitorData.productNumber
      ) !== undefined

    if (monitorAlreadyExists)
      return Promise.reject(
        `Sorry, the product number "${monitorData.productNumber}" is already in use.`
      )

    createMonitorApi(monitorData)
      .then(response =>
        setMonitors(previousMonitors => [...previousMonitors, response.data])
      )
      .catch(console.log)

    return Promise.resolve()
  }

  const handleToggleFilter: ToggleFilterFunctionType = fieldName => event => {
    setMonitorFilters(previousMonitorFilters => ({
      ...previousMonitorFilters,
      [fieldName]: {
        ...previousMonitorFilters[fieldName],
        active: event.target.checked,
      },
    }))
  }

  const handleChangeFilterValue: ChangeFilterValueFunctionType =
    fieldName => event => {
      setMonitorFilters(previousMonitorFilters => ({
        ...previousMonitorFilters,
        [fieldName]: {
          ...previousMonitorFilters[fieldName],
          value: event.target.value,
        },
      }))
    }

  // const visibleMonitors = (): MonitorType[] => {
  //   return FILTERS_DATA.reduce(
  //     (acc, [filterName, fieldName, filterFn]) =>
  //       monitorFilters[filterName].active &&
  //       monitorFilters[filterName].value !== ''
  //         ? acc.filter(monitor =>
  //             filterFn(monitor[fieldName], monitorFilters[filterName].value)
  //           )
  //         : acc,
  //     monitors
  //   )
  // }

  // const visibleMonitors = (): MonitorType[] => {
  //   const { brand, minimumScreenSize, maximumPrice } = monitorFilters

  //   return monitors.filter(
  //     monitor =>
  //       (brand.active && brand.value !== ''
  //         ? monitor.brand === brand.value
  //         : true) &&
  //       (minimumScreenSize.active && minimumScreenSize.value !== ''
  //         ? monitor.screenSize >= minimumScreenSize.value
  //         : true) &&
  //       (maximumPrice.active && maximumPrice.value !== ''
  //         ? monitor.price <= maximumPrice.value
  //         : true)
  //   )
  // }

  const visibleMonitors = (): MonitorType[] => {
    let filteredMonitors = inStockMonitors()

    if (monitorFilters.brand.active && monitorFilters.brand.value !== '')
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.brandId === +monitorFilters.brand.value
      )

    if (
      monitorFilters.minimumScreenSize.active &&
      monitorFilters.minimumScreenSize.value !== ''
    )
      filteredMonitors = filteredMonitors.filter(
        monitor =>
          monitor.screenSize.value >= +monitorFilters.minimumScreenSize.value
      )

    if (
      monitorFilters.maximumPrice.active &&
      monitorFilters.maximumPrice.value !== ''
    )
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.price <= +monitorFilters.maximumPrice.value
      )

    return filteredMonitors
  }

  const handleSellMonitor: SellMonitorFunctionType = productNumber => {
    const soldMonitor = monitors.find(
      monitor => monitor.productNumber === productNumber && monitor.quantity > 0
    )

    if (!soldMonitor)
      return Promise.reject(
        `Monitor with product number '${productNumber}' not found or is out of stock`
      )

    sellMonitorApi(soldMonitor.id).then(response => {
      setMonitors(previousMonitors =>
        previousMonitors
          .map(monitor => (monitor === soldMonitor ? response.data : monitor))
          .filter(monitor => monitor.quantity > 0)
      )
    })

    return Promise.resolve(soldMonitor.quantity - 1)
  }

  return (
    <>
      <h1>{storeName}</h1>

      <NewMonitorForm onAddMonitor={handleAddMonitor} />
      <MonitorFilters
        monitorFilters={monitorFilters}
        onToggleFilter={handleToggleFilter}
        onChangeFilterValue={handleChangeFilterValue}
        monitors={monitors}
      />
      <MonitorInventory
        monitors={visibleMonitors()}
        onSellMonitor={handleSellMonitor}
      />
    </>
  )
}
