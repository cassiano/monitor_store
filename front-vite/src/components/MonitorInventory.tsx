import { MonitorType, SellMonitorFunctionType } from './MonitorStore'
import { useState } from 'react'
import React from 'react'

type MonitorInventoryProps = {
  monitors: MonitorType[]
  onSellMonitor: SellMonitorFunctionType
}

export const MonitorInventory: React.FC<MonitorInventoryProps> = ({
  monitors,
  onSellMonitor,
}) => {
  const [selectedMonitorProductNumber, setSelectedMonitorProductNumber] =
    useState<string | null>(null)

  const handleSellMonitorClick = (_event: React.MouseEvent<HTMLElement>) => {
    if (
      window.confirm(
        `Are you sure you want to sell monitor '${selectedMonitorProductNumber}'?`
      )
    ) {
      onSellMonitor(selectedMonitorProductNumber!)
        .then((updatedQuantity: number) => {
          if (updatedQuantity === 0) setSelectedMonitorProductNumber(null)
        })
        .catch(err => alert(err))
    }
  }

  return (
    <>
      <br />

      <table border={1} cellPadding='3' style={{ fontSize: 12 }}>
        <thead>
          <tr>
            <th>Product Number</th>
            <th>Brand</th>
            <th>Screen Size</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          {monitors.map(monitor => (
            <tr
              style={{
                borderColor:
                  selectedMonitorProductNumber === monitor.productNumber
                    ? 'red'
                    : 'white',
              }}
              key={monitor.productNumber}
              onClick={_event =>
                setSelectedMonitorProductNumber(monitor.productNumber)
              }
            >
              <td>{monitor.productNumber}</td>
              <td>{monitor.brand.name}</td>
              <td>{monitor.screenSize.value}</td>
              <td>{monitor.price}</td>
              <td>{monitor.quantity}</td>
            </tr>
          ))}
        </tbody>
      </table>

      {selectedMonitorProductNumber !== null && (
        <input
          type='button'
          value={`Sell '${selectedMonitorProductNumber}' Monitor`}
          onClick={handleSellMonitorClick}
        />
      )}
    </>
  )
}
