import { useState } from 'react'
import { AddMonitorFunctionType, MonitorType, BrandType } from './MonitorStore'
import { useBrandsLoader, useScreenSizesLoader } from '../services/loaders'
import { ScreenSizeType } from './MonitorStore'

// Partial<MonitorType>
type MonitorDefaultFormDataType = {
  productNumber: string
  brandId: BrandType['id'] | ''
  screenSizeId: ScreenSizeType['id'] | ''
  price: number | ''
  quantity: number | ''
}

const MONITOR_DEFAULT_FORM_DATA: MonitorDefaultFormDataType = {
  productNumber: '',
  brandId: '',
  screenSizeId: '',
  price: '',
  quantity: '',
}

type NewMonitorFormProps = {
  onAddMonitor: AddMonitorFunctionType
}

export const NewMonitorForm: React.FC<NewMonitorFormProps> = ({
  onAddMonitor,
}) => {
  const [monitorFormData, setMonitorFormData] = useState(
    MONITOR_DEFAULT_FORM_DATA
  )

  const { brands } = useBrandsLoader()
  const { screenSizes } = useScreenSizesLoader()

  const handleAddMonitorClick = (_event: React.MouseEvent<HTMLElement>) => {
    onAddMonitor(monitorFormData as MonitorType)
      .then(() =>
        // Monitor added sucessfully! Reset the form.
        setMonitorFormData(MONITOR_DEFAULT_FORM_DATA)
      )
      .catch(err => alert(err))
  }

  return (
    <fieldset>
      <legend>Add to inventory</legend>

      <label htmlFor='productNumber'>Product Number: </label>
      <input
        type='text'
        name='productNumber'
        value={monitorFormData.productNumber}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            productNumber: event.target.value,
          })
        }
      />
      <br />

      <label htmlFor='brand'>Brand: </label>
      <select
        name='brand'
        value={monitorFormData.brandId}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            brandId: event.target
              .value as MonitorDefaultFormDataType['brandId'],
          })
        }
      >
        <option value=''>-- Select --</option>
        {brands.map(brand => (
          <option key={brand.id} value={brand.id}>
            {brand.name}
          </option>
        ))}
      </select>
      <br />

      <label htmlFor='screenSize'>Screen Size: </label>
      <select
        name='screenSize'
        value={monitorFormData.screenSizeId}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            screenSizeId: parseInt(
              event.target.value
            ) as MonitorDefaultFormDataType['screenSizeId'],
          })
        }
      >
        <option disabled={true} value=''>
          -- Select --
        </option>
        {screenSizes.map(screenSize => (
          <option key={screenSize.id} value={screenSize.id}>
            {screenSize.value}
          </option>
        ))}
      </select>
      <br />

      <label htmlFor='price'>Price: </label>
      <input
        type='number'
        name='price'
        value={monitorFormData.price}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            price: event.target.value !== '' ? +event.target.value : '',
          })
        }
      />
      <br />

      <label htmlFor='quantity'>Quantity: </label>
      <input
        type='number'
        name='quantity'
        value={monitorFormData.quantity}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            quantity: event.target.value !== '' ? +event.target.value : '',
          })
        }
      />
      <br />

      <input
        type='button'
        value='Add Monitor'
        onClick={handleAddMonitorClick}
        disabled={
          !(
            monitorFormData.productNumber !== '' &&
            monitorFormData.brandId !== '' &&
            monitorFormData.price !== '' &&
            monitorFormData.price > 0 &&
            monitorFormData.quantity !== '' &&
            monitorFormData.quantity > 0
          )
        }
      />
    </fieldset>
  )
}
