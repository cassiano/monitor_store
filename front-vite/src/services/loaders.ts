import { useEffect, useState } from 'react'
import { listBrandsApi, listScreenSizesApi, listMonitorsApi } from './api'
import {
  BrandType,
  ScreenSizeType,
  MonitorType,
} from '../components/MonitorStore'

export const useBrandsLoader = () => {
  const [brands, setBrands] = useState<BrandType[]>([])

  useEffect(() => {
    listBrandsApi()
      .then(response => setBrands(response.data))
      .catch(console.log)
  }, [])

  return { brands, setBrands }
}

export const useScreenSizesLoader = () => {
  const [screenSizes, setScreenSizes] = useState<ScreenSizeType[]>([])

  useEffect(() => {
    listScreenSizesApi()
      .then(response => setScreenSizes(response.data))
      .catch(console.log)
  }, [])

  return { screenSizes, setScreenSizes }
}

export const useMonitorsLoader = () => {
  const [monitors, setMonitors] = useState<MonitorType[]>([])

  useEffect(() => {
    listMonitorsApi()
      .then(response => setMonitors(response.data))
      .catch(console.log)
  }, [])

  return { monitors, setMonitors }
}
