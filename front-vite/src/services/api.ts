import axios from 'axios'
import {
  BrandType,
  MonitorType,
  ScreenSizeType,
} from '../components/MonitorStore'

axios.defaults.baseURL = 'http://localhost:3001/api'

export const listBrandsApi = () => axios.get<BrandType[]>('/brands')

export const listScreenSizesApi = () =>
  axios.get<ScreenSizeType[]>('/screen_sizes')

export const listMonitorsApi = () =>
  axios.get<MonitorType[]>('/computer_monitors')
export const createMonitorApi = (attrs: MonitorType) =>
  axios.post<MonitorType>(`/computer_monitors`, attrs)
export const sellMonitorApi = (id: MonitorType['id']) =>
  axios.patch<MonitorType>(`/computer_monitors/${id}/sell`)
