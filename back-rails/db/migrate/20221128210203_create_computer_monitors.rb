class CreateComputerMonitors < ActiveRecord::Migration[7.0]
  def change
    create_table :computer_monitors do |t|
      t.references :brand, null: false, foreign_key: true
      t.references :screen_size, null: false, foreign_key: true
      t.string :product_number, null: false, index: { unique: true }
      t.decimal :price, null: false, precision: 11, scale: 2
      t.integer :quantity, null: false

      t.timestamps
    end
  end
end
