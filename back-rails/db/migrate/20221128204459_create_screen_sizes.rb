class CreateScreenSizes < ActiveRecord::Migration[7.0]
  def change
    create_table :screen_sizes do |t|
      t.integer :value, null: false, index: { unique: true }

      t.timestamps
    end
  end
end
