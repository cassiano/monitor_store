# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Brand.create name: 'IBM'
Brand.create name: 'HP'
Brand.create name: 'Lenovo'
Brand.create name: 'Dell'

ScreenSize.create value: 17
ScreenSize.create value: 19
ScreenSize.create value: 21
ScreenSize.create value: 23
ScreenSize.create value: 25
