# == Schema Information
#
# Table name: screen_sizes
#
#  id         :integer          not null, primary key
#  value      :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_screen_sizes_on_value  (value) UNIQUE
#
class ScreenSize < ApplicationRecord
  validates :value, presence: true, uniqueness: true

  has_many :computer_monitors
end
