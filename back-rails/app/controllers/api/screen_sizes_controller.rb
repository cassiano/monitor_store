class Api::ScreenSizesController < Api::ApiController
  def index
    render json: ScreenSize.all.order(value: :asc)
  end
end
