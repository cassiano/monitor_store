class Api::BrandsController < Api::ApiController
  def index
    render json: Brand.all.order(name: :asc)
  end
end
