class Api::ComputerMonitorsController < Api::ApiController
  def index
    render json: ComputerMonitor.in_stock
  end

  def create
    computer_monitor = ComputerMonitor.new(create_params)

    if computer_monitor.save
      render json: computer_monitor, status: :created  # 201 = Created
    else
      render json: computer_monitor.errors.full_messages, status: :unprocessable_entity  # 422 = Unprocessable Entity
    end
  end

  def sell
    computer_monitor = ComputerMonitor.find(params[:id])
    computer_monitor.quantity -= 1

    if computer_monitor.quantity == 0
      computer_monitor.destroy
    else
      if computer_monitor.save
        render json: computer_monitor
      else
        render json: computer_monitor.errors.full_messages, status: :unprocessable_entity  # 422 = Unprocessable Entity
      end
    end
  end

  private

  def create_params
    # Whitelist
    params.permit :product_number, :brand_id, :screen_size_id, :price, :quantity
  end
end
