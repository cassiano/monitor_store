require "test_helper"

class Api::ScreenSizesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_screen_sizes_index_url
    assert_response :success
  end
end
