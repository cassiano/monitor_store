require "test_helper"

class Api::ComputerMonitorsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_computer_monitors_index_url
    assert_response :success
  end
end
