# == Schema Information
#
# Table name: computer_monitors
#
#  id             :integer          not null, primary key
#  price          :decimal(11, 2)   not null
#  product_number :string           not null
#  quantity       :integer          not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  brand_id       :integer          not null
#  screen_size_id :integer          not null
#
# Indexes
#
#  index_computer_monitors_on_brand_id        (brand_id)
#  index_computer_monitors_on_product_number  (product_number) UNIQUE
#  index_computer_monitors_on_screen_size_id  (screen_size_id)
#
# Foreign Keys
#
#  brand_id        (brand_id => brands.id)
#  screen_size_id  (screen_size_id => screen_sizes.id)
#
require "test_helper"

class ComputerMonitorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
