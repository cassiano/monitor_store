# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

# Defines the root path route ("/")
# root "articles#index"
Rails.application.routes.draw do
  namespace :api do
    resources :brands, only: [:index]
    resources :screen_sizes, only: [:index]
    resources :computer_monitors, only: [:index, :create] do
      patch :sell, on: :member
    end
  end
end
