bundle add rack_cors
bundle add active_model_serializers

rails g model brand name
rails g model screen_size value:integer
rails g model computer_monitor product_number brand:references screen_size:references price:decimal quantity:integer
rails g controller api/brands index
rails g controller api/screen_sizes index
rails g controller api/computer_monitors index create sell

rails g serializer brand
rails g serializer screen_size
rails g serializer computer_monitor
