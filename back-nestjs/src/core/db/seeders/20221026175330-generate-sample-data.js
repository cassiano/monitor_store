'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, _Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      'Brands',
      [
        {
          name: 'IBM',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'HP',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Lenovo',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Dell',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );

    await queryInterface.bulkInsert(
      'ScreenSizes',
      [
        {
          value: 17,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          value: 19,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          value: 21,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          value: 23,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          value: 25,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );
  },

  async down(queryInterface, _Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete('Brands', null, {});
    await queryInterface.bulkDelete('Sizes', null, {});
  },
};
