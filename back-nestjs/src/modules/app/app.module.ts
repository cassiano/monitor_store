import { Module } from '@nestjs/common';
import { BrandsModule } from '../brands/brands.module';
import { MonitorsModule } from '../monitors/monitors.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScreenSizesModule } from '../screenSizes/screenSizes.module';

@Module({
  imports: [MonitorsModule, BrandsModule, ScreenSizesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
