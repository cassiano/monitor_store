import { Test, TestingModule } from '@nestjs/testing';
import { ScreenSizesService } from './screenSizes.service';

describe('ScreenSizesService', () => {
  let service: ScreenSizesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ScreenSizesService],
    }).compile();

    service = module.get<ScreenSizesService>(ScreenSizesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
