import { PartialType } from '@nestjs/mapped-types';
import { CreateScreenSizeDto } from './create-screenSize.dto';

export class UpdateScreenSizeDto extends PartialType(CreateScreenSizeDto) {}
