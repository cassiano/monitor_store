import {
  Table,
  Column,
  Model,
  PrimaryKey,
  HasMany,
  AutoIncrement,
} from 'sequelize-typescript';
import { IsNotEmpty } from 'class-validator';
import { Monitor } from '../../monitors/entities/monitor.entity';

@Table({ timestamps: true })
export class ScreenSize extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @IsNotEmpty()
  @Column
  value: number;

  @IsNotEmpty()
  @Column
  createdAt: Date;

  @IsNotEmpty()
  @Column
  updatedAt: Date;

  @HasMany(() => Monitor)
  monitors: Monitor[];
}
