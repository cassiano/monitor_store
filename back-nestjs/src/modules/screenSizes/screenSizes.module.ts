import { Module } from '@nestjs/common';
import { ScreenSizesService } from './screenSizes.service';
import { ScreenSizesController } from './screenSizes.controller';
import { screenSizesProviders } from './screenSizes.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [ScreenSizesController],
  providers: [ScreenSizesService, ...screenSizesProviders],
})
export class ScreenSizesModule {}
