import {
  Controller,
  Get,
  // Post,
  // Body,
  // Patch,
  // Param,
  // Delete,
} from '@nestjs/common';
import { ScreenSizesService } from './screenSizes.service';
// import { CreateSizeDto } from './dto/create-size.dto';
// import { UpdateSizeDto } from './dto/update-size.dto';

@Controller('screenSizes')
export class ScreenSizesController {
  constructor(private readonly screenSizesService: ScreenSizesService) {}

  // @Post()
  // create(@Body() createSizeDto: CreateSizeDto) {
  //   return this.sizesService.create(createSizeDto);
  // }

  @Get()
  findAll() {
    return this.screenSizesService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.sizesService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateSizeDto: UpdateSizeDto) {
  //   return this.sizesService.update(+id, updateSizeDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.sizesService.remove(+id);
  // }
}
