import { Test, TestingModule } from '@nestjs/testing';
import { ScreenSizesController } from './screenSizes.controller';
import { ScreenSizesService } from './screenSizes.service';

describe('SizesController', () => {
  let controller: ScreenSizesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ScreenSizesController],
      providers: [ScreenSizesService],
    }).compile();

    controller = module.get<ScreenSizesController>(ScreenSizesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
