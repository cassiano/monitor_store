import { SCREEN_SIZES_REPOSITORY } from '../../core/constants';
import { ScreenSize } from './entities/screenSize.entity';

export const screenSizesProviders = [
  {
    provide: SCREEN_SIZES_REPOSITORY,
    useValue: ScreenSize,
  },
];
