import { Inject, Injectable } from '@nestjs/common';
import { SCREEN_SIZES_REPOSITORY } from 'src/core/constants';
import { ScreenSize } from './entities/screenSize.entity';
// import { CreateSizeDto } from './dto/create-size.dto';
// import { UpdateSizeDto } from './dto/update-size.dto';

@Injectable()
export class ScreenSizesService {
  constructor(
    @Inject(SCREEN_SIZES_REPOSITORY)
    private readonly screenSizesRepository: typeof ScreenSize,
  ) {}

  // create(createSizeDto: CreateSizeDto) {
  //   return 'This action adds a new size';
  // }

  findAll() {
    // return `This action returns all sizes`;
    return this.screenSizesRepository.findAll();
  }

  // findOne(id: number) {
  //   return `This action returns a #${id} size`;
  // }

  // update(id: number, updateSizeDto: UpdateSizeDto) {
  //   return `This action updates a #${id} size`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} size`;
  // }
}
