import { MONITORS_REPOSITORY } from 'src/core/constants';
import { Monitor } from './entities/monitor.entity';

export const monitorsProviders = [
  {
    provide: MONITORS_REPOSITORY,
    useValue: Monitor,
  },
];
