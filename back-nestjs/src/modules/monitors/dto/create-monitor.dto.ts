export class CreateMonitorDto {
  productNumber: string;
  brandId: number;
  quantity: number;
  screenSizeId: number;
  price: number;

  // ???
  [index: number]: any;
  [index: symbol]: any;
}
