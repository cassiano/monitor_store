import {
  Table,
  Column,
  Model,
  PrimaryKey,
  BelongsTo,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
  AutoIncrement,
} from 'sequelize-typescript';
import { IsNotEmpty } from 'class-validator';
import { Brand } from '../../brands/entities/brand.entity';
import { ScreenSize } from '../../screenSizes/entities/screenSize.entity';

@Table({ timestamps: true })
export class Monitor extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @IsNotEmpty()
  @Column
  productNumber: string;

  @IsNotEmpty()
  @ForeignKey(() => Brand)
  @Column
  brandId: number;

  @IsNotEmpty()
  @ForeignKey(() => ScreenSize)
  @Column
  screenSizeId: number;

  @IsNotEmpty()
  @Column
  quantity: number;

  @IsNotEmpty()
  @Column
  price: number;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @BelongsTo(() => Brand)
  brand: Brand;

  @BelongsTo(() => ScreenSize)
  screenSize: ScreenSize;
}
