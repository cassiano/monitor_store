import { Module } from '@nestjs/common';
import { MonitorsService } from './monitors.service';
import { MonitorsController } from './monitors.controller';
import { DatabaseModule } from 'src/modules/database/database.module';
import { monitorsProviders } from './monitors.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [MonitorsController],
  providers: [MonitorsService, ...monitorsProviders],
})
export class MonitorsModule {}
