import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  // Delete,
} from '@nestjs/common';
import { MonitorsService } from './monitors.service';
import { CreateMonitorDto } from './dto/create-monitor.dto';
// import { UpdateMonitorDto } from './dto/update-monitor.dto';

@Controller('monitors')
export class MonitorsController {
  constructor(private readonly monitorsService: MonitorsService) {}

  @Get()
  findAll() {
    return this.monitorsService.findAll();
  }

  @Post()
  create(@Body() monitor: CreateMonitorDto) {
    return this.monitorsService.create(monitor);
  }

  // @Get(':id')
  // findOne(@Param('id') id: number) {
  //   return this.monitorsService.findOne(id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: number, @Body() monitor: UpdateMonitorDto) {
  //   return this.monitorsService.update(id, monitor);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: number) {
  //   return this.monitorsService.remove(+id);
  // }

  @Patch(':productNumber/sell')
  update(@Param('productNumber') productNumber: string) {
    return this.monitorsService.sell(productNumber);
  }
}
