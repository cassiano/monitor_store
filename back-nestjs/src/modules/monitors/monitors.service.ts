import { Inject, Injectable } from '@nestjs/common';
import { CreateMonitorDto } from './dto/create-monitor.dto';
// import { UpdateMonitorDto } from './dto/update-monitor.dto';
import { Monitor } from './entities/monitor.entity';
import { MONITORS_REPOSITORY } from '../../core/constants';
import { Brand } from '../brands/entities/brand.entity';
import { ScreenSize } from '../screenSizes/entities/screenSize.entity';

@Injectable()
export class MonitorsService {
  constructor(
    @Inject(MONITORS_REPOSITORY)
    private readonly monitorsRepository: typeof Monitor,
  ) {}

  findAll() {
    return this.monitorsRepository.findAll({ include: [Brand, ScreenSize] });
  }

  async create(monitor: CreateMonitorDto) {
    const newMonitor = await this.monitorsRepository.create(monitor);

    return this.monitorsRepository.findByPk(newMonitor.id, {
      include: [Brand, ScreenSize],
    });
  }

  // findOne(id: number) {
  //   return this.monitorsRepository.findOne({ where: { id } });
  // }

  // update(id: number, monitor: UpdateMonitorDto) {
  //   return `This action updates a #${id} monitor`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} monitor`;
  // }

  async sell(productNumber: string) {
    const monitor = await this.monitorsRepository.findOne({
      where: { productNumber },
      include: [Brand, ScreenSize],
    });

    if (monitor) return monitor.update({ quantity: monitor.quantity - 1 });

    return monitor;
  }
}
