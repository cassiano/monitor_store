import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/modules/database/database.module';
import { BrandsController } from './brands.controller';
import { BrandsService } from './brands.service';
import { brandsProviders } from './brands.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [BrandsController],
  providers: [BrandsService, ...brandsProviders],
})
export class BrandsModule {}
