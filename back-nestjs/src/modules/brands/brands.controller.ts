import { BrandsService } from './brands.service';
import { Controller, Get } from '@nestjs/common';

@Controller('brands')
export class BrandsController {
  constructor(private readonly brandsService: BrandsService) {}

  @Get()
  findAll() {
    return this.brandsService.findAll();
  }
}
