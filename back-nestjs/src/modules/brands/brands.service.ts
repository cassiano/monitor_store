import { Inject, Injectable } from '@nestjs/common';
import { BRANDS_REPOSITORY } from '../../core/constants';
import { Brand } from './entities/brand.entity';

@Injectable()
export class BrandsService {
  constructor(
    @Inject(BRANDS_REPOSITORY)
    private readonly brandsRepository: typeof Brand,
  ) {}

  findAll() {
    return this.brandsRepository.findAll();
  }
}
