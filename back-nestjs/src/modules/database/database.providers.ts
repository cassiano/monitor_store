import { Sequelize } from 'sequelize-typescript';
import { Brand } from '../brands/entities/brand.entity';
import { Monitor } from '../monitors/entities/monitor.entity';
import { ScreenSize } from '../screenSizes/entities/screenSize.entity';

// const env = process.env.NODE_ENV || 'development';
// const config = databaseJson[env];

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'sqlite',
        storage: './src/core/db/development.sqlite',
      });
      sequelize.addModels([Monitor, Brand, ScreenSize]);
      // await sequelize.sync();
      return sequelize;
    },
  },
];
